package edu.uchicago.jung.prowebservice;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class NewsListingActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<News>>{

    private NewsAdapter mAdapter;
    private String mKeyword;
    private static final int NEWS_LOADER_ID = 1;
    public static final String QUERY_URL = "http://content.guardianapis.com/search?show-tags=contributor&q=";
    public static final String API_KEY = "&api-key=eac3f007-d497-4ba0-ad1c-3682ae527f1c";
    public static String loaderURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_listing);
        //create api query url
        mKeyword = getIntent().getExtras().getString("keyword");
        loaderURL = QUERY_URL+mKeyword;

        //start up loader
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(NEWS_LOADER_ID, null, this);
        //hook up listview with adapter
        ListView listView = (ListView) findViewById(R.id.list);
        mAdapter = new NewsAdapter(this, new ArrayList<News>());
        listView.setAdapter(mAdapter);
        //when item is clicked, it takes user to the news website
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News currentNews = mAdapter.getItem(position);
                Uri newsUri = null;
                if (currentNews != null) {
                    newsUri = Uri.parse(currentNews.getUrl());
                }
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, newsUri);
                startActivity(websiteIntent);
            }
        });

    }

    @Override
    public Loader<List<News>> onCreateLoader(int id, Bundle args) {

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String startYear = sharedPrefs.getString(
                getString(R.string.settings_year_key),
                getString(R.string.settings_year_default));

        String startMonth = sharedPrefs.getString(
                getString(R.string.setting_month_key),
                getString(R.string.setting_month_default));

        String startDay = sharedPrefs.getString(
                getString(R.string.setting_day_key),
                getString(R.string.setting_day_default));

        String searchDate = createDateFormat(startYear, startMonth, startDay);

        Uri baseUri = Uri.parse(loaderURL);
        Uri.Builder uriBuilder = baseUri.buildUpon();

        uriBuilder.appendQueryParameter("from-date", searchDate);
        uriBuilder.appendQueryParameter("order-by", "newest");
        uriBuilder.appendQueryParameter("page-size", "150");

        return new NewsLoader(this,uriBuilder.toString()+API_KEY);
    }

    @Override
    public void onLoadFinished(Loader<List<News>> loader, List<News> newses) {
        mAdapter.clear();
        if (newses != null && !newses.isEmpty()){
            mAdapter.addAll(newses);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<News>> loader) {
        mAdapter.clear();
    }

    private String createDateFormat(String year, String month, String day){
        return year + "-" + month + "-" + day;
    }
}
