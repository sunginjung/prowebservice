package edu.uchicago.jung.prowebservice;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    private Button mKoreaButton;
    private Button mTrumpButton;
    private Button mSportsButton;
    private Button mChicagoButton;

    private String mKeyword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mChicagoButton = (Button)findViewById(R.id.button_chicago);
        mKoreaButton = (Button)findViewById(R.id.button_korea);
        mTrumpButton = (Button)findViewById(R.id.button_trump);
        mSportsButton = (Button)findViewById(R.id.button_sports);

        //set up listeners
        mChicagoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNews("chicago");
            }
        });

        mKoreaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNews("korea");
            }
        });

        mTrumpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNews("trump");
            }
        });

        mSportsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNews("sports");
            }
        });
    }

    private void showNews(String keyword){
        final ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        //see if internet connection is available
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()){

            try {
                mKeyword = URLEncoder.encode(keyword,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Intent newsIntent = new Intent(MainActivity.this, NewsListingActivity.class);
            newsIntent.putExtra("keyword", mKeyword);
            startActivity(newsIntent);
        } else{
            Toast.makeText(MainActivity.this,"You sure you have internet?",Toast.LENGTH_SHORT).show();
        }
    }

    //for user setting for search period
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
